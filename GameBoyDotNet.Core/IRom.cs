﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBoyDotNet.Core {

	/// <summary>
	/// Represents a read-only memory bank. Can be the BIOS or a cartridge mapping
	/// </summary>
	public interface IRom {
		byte ReadByte(ushort address);
	}
}
