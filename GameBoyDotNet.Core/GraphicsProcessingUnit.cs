﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBoyDotNet.Core {
	public interface IGraphicsProcessingUnit {
		byte ReadByteFromVideoRam(ushort address);
		byte ReadByteFromSpriteAttributeTable(ushort address);
	}

	public class GraphicsProcessingUnit : IGraphicsProcessingUnit {
		private byte[] _videoRam = new byte[8 * 1024];
		public byte[] _spriteAttributeTable = new byte[256];

		public byte ReadByteFromVideoRam(ushort address) {
			return _videoRam[address];
		}

		public byte ReadByteFromSpriteAttributeTable(ushort address) {
			return _spriteAttributeTable[address];
		}

	}
}
