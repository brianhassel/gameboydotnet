﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBoyDotNet.Core {
	public interface IJoyPad {
		void WriteByte(byte value);
		byte ReadByte();
	}

	/// <summary>
	/// The joypad requires a write to FF00 to select the button set, then a read to get the pressed buttons:
	/// 
	/// FF00 - P1/JOYP - Joypad (R/W)
	///The eight gameboy buttons/direction keys are arranged in form of a 2x4 matrix. Select either button or direction keys by writing to this register, then read-out bit 0-3.
	///  Bit 7 - Not used
	///  Bit 6 - Not used
	///  Bit 5 - P15 Select Button Keys      (0=Select)
	///  Bit 4 - P14 Select Direction Keys   (0=Select)
	///  Bit 3 - P13 Input Down  or Start    (0=Pressed) (Read Only)
	///  Bit 2 - P12 Input Up    or Select   (0=Pressed) (Read Only)
	///  Bit 1 - P11 Input Left  or Button B (0=Pressed) (Read Only)
	///  Bit 0 - P10 Input Right or Button A (0=Pressed) (Read Only)
	///Note: Most programs are repeatedly reading from this port several time
	/// 
	/// </summary>
	public class JoyPad : IJoyPad {

		[Flags]
		public enum ButtonsPressed : byte {
			A = 0x00,
			B = 0x01,
			Select = 0x02,
			Start = 0x04,
			Right = 0x08,
			Left = 0x10,
			Up = 0x20,
			Down = 0x40
		}

		public void WriteByte(byte value) {
			//Bit 4 or Bit 5 determines button mode (0 for selected)
			_selectDirectionKeys = (value & 0x10) != 0x10;
			_selectButtonKeys = (value & 0x20) != 0x20;
		}

		public byte ReadByte() {
			if (_selectButtonKeys) {
				return (byte) ((byte) _buttonsPressed & 0x0F);
			}
			if(_selectDirectionKeys) {
				return (byte) (((byte) _buttonsPressed & 0xF0) >> 4);
			}
			return 0;
		}

		public void PressButtons(ButtonsPressed buttonsPressed) {
			_buttonsPressed = buttonsPressed;
		}

		private bool _selectDirectionKeys;
		private bool _selectButtonKeys;
		private ButtonsPressed _buttonsPressed;

	}
}
