﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBoyDotNet.Core {
	/// <summary>
	/// The MMU has 16-bit addresses for the following
	/// 
	///  0000-3FFF   16KB ROM Bank 00     (in cartridge, fixed at bank 00)
	///  4000-7FFF   16KB ROM Bank 01..NN (in cartridge, switchable bank number)
	///  8000-9FFF   8KB Video RAM (VRAM) (switchable bank 0-1 in CGB Mode)
	///  A000-BFFF   8KB External RAM     (in cartridge, switchable bank, if any)
	///  C000-CFFF   4KB Work RAM Bank 0 (WRAM)
	///  D000-DFFF   4KB Work RAM Bank 1 (WRAM)  (switchable bank 1-7 in CGB Mode)
	///  E000-FDFF   Same as C000-DDFF (ECHO)    (typically not used)
	///  FE00-FE9F   Sprite Attribute Table (OAM)
	///  FEA0-FEFF   Not Usable
	///  FF00-FF7F   I/O Ports
	///  FF80-FFFE   High RAM (HRAM)
	///  FFFF        Interrupt Enable Register
	/// 
	/// </summary>
	public class MemoryManagementUnit {
		private readonly IJoyPad _joyPad;
		private readonly IGraphicsProcessingUnit _gpu;

		private byte[] _highRam = new byte[256];
		private byte[] _workRam = new byte[8 * 1024];
		

		public MemoryManagementUnit(IJoyPad joyPad, IGraphicsProcessingUnit gpu) {
			_joyPad = joyPad;
			_gpu = gpu;
		}


		public byte ReadByte(ushort address) {
			if (address <= 0x7FFF || //Cartridge ROM Bank 
				(address >= 0xA000 && address <= 0xBFFF)) //8KB External RAM in cart
				{
				return Rom.ReadByte(address);
			} else if (address >= 0x8000 && address <= 0x9FFF) { //8KB Video RAM
				return _gpu.ReadByteFromVideoRam((ushort)(address - 0x8000));
			} else if (address >= 0xC000 && address <= 0xDFFF) { //8KB Work RAM (no switching, only original GB
				return _workRam[address - 0xC000];
			} else if (address >= 0xE000 && address <= 0xFDFF) { //A duplicate mapping of Work RAM
				return _workRam[address - 0xE000];
			} else if (address >= 0xFE00 && address <= 0xFEFF) { //Sprite Attribute Table (OAM)
				return _gpu.ReadByteFromSpriteAttributeTable((ushort)(address - 0xFE00));
			} else if (address >= 0xFF80 && address <= 0xFFFE) { //256B High RAM (HRAM)
				return _highRam[0xFF & address];
			}

			//JoyPad
			if (address == 0xFF00) {
				return _joyPad.ReadByte();
			} 

			throw new Exception("Invalid address: " + address.ToString("X4"));
		}

		public void WriteByte(ushort address, byte value) {
			
		}


		public void WriteWord(ushort address, ushort value) {
			WriteByte(address, (byte)(value & 0xFF));
			WriteByte((ushort)(address + 1), (byte)(value >> 8));
		}

		public ushort ReadWord(ushort address) {
			int low = ReadByte(address);
			int high = ReadByte((ushort) (address + 1));
			return (ushort)((high << 8) | low);
		}


		private IRom Rom
		{
			get { return null; }
		}


	}
}
